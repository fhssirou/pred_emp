# Rapport sur l’étude du bien-être en entreprise et des départs anticipés des salariés

|     Auteurs        |
--------------
|    HISSIROU Fodé & BOUSSAA Mohamed       |

## Introduction 

### a) Contexte : description du projet

Constat actuel :  qu’un départ salarié soit qualifié de démission ou qu’il soit requalifié de licenciement, porte sur la nature du contrat et ne constitue donc qu’un problème juridique. Néanmoins la conséquence n’en restera pas moins celle d’un départ salarié. Aussi, si le problème demeurera inexorablement, c’est que l’incidence d’un départ salarié impactera aussi bien l’entreprise que le salarié lui-même.


En effet, l’acquisition d’un salarié pour une entreprise représente un coût qui affecte plusieurs de ses ressources, tant sur le plan financier qu’humain. 


Sur le marché du travail, les outputs que l’on nomme les personnes extérieures à l’entreprise sont les offreurs de travail. Aussi, s’il existe selon l’orthodoxie économique une asymétrie d’information au sein du marché du travail, c’est parce qu’elles doivent arbitrer entre les outputs pour savoir lequel d’entre eux saura entre autre s’inscrire comme un facteur de production optimal, et en écartant - les passagers clandestins – selon la théorie économique – les salariés qui ne maximisent pas leur effort en entreprise –.


En l’espèce, la recherche d’un employé représente un coût depuis le dépôt des fiches de poste, au passage d’entretien de recrutement, à l’intégration du salarié, jusqu’à sa formation et son développement au sein des équipes. Le temps et les compétences dépensées à servir cet objectif, conjuguées à l’investissement en matière de formation représentent un coût financier pesant lourdement sur les charges de l’entreprise.


En sommes, le turn over de nombreuses structures représente outre un gouffre financier, et une perte d’élan de la productivité des services, une détérioration de la qualité de l’environnement de travail.

Est-ce qu’un environnement de travail de qualité offre des conditions d’épanouissement d’un salarié et par cela, un déterminant majeur de la pérennité d’un contrat de travail ? La législation française promeut et légifère continuellement des textes de loi en vue d’améliorer la qualité « des relations individuelles et collectives qui naissent entre les employeurs et ceux qui travaillent sous leur autorité à l’occasion de ce travail ».


### b) Objectif:

L’objectif de notre projet consiste à utiliser une évaluation du bien être en entreprise afin de prédire des corrélations avec d’éventuels départs anticipés, qu’il s’agisse de personnes en fonction ou qu’il s’agisse de personne possédant au moins une expérience en entreprise.

### c) Intérêt de notre recherche : 

L'intérêt de cette démarche consiste à optimiser non seulement la sélection à l'embauche, mais aussi de permettre l'amélioration de l'accompagnement des collaborateurs au sein de leur environnement de travail.


Grâce à cela, le coût que représente le départ d'un collaborateur sera réduit, un gain de temps, une économie financière et une optimisation de la coordination des équipes pourra être un axe développement des services.

Analyser les données des collaborateurs ayant déjà quitté leur poste est une piste de travail et de recherche sur laquelle le service des Ressources Humaines peut trouver des analogies avec les actuels collaborateurs, autrement dit autant d’indicateurs à identifier chez les employés actuellement en poste, dans le but de repérer d’éventuelles similarités avec les précédents départs salariés. 


Nous montrerons la corrélation entre départs anticipés et qualité du bien-être en entreprise. Enfin nous présenterons la corrélation du profil de personnalité et l'environnement de travail. Ces prédictions seront traitées à partir d’algorithmes différents de Machine Learning sans omettre une comparaison avec l’utilisation de la corrélation de Pearson. 


## Plan : 
1. Méthodologie : explication des méthodes sélectionnées et détail des étapes rencontrées.
2. Expression d'un cahier des charges fonctionnel et technique : présentation du fonctionnement des technologies.
3. Rétroplanning & distribution des rôles : graphique et plan des étapes.
4. Solutions et retours d'expériences : problèmes rencontrés et solutions proposées.
5. Conclusion et limite.

## I. Méthodologie fonctionnelle

Notre première réflexion quant au choix des technologies, a été d’aller au-delà des exigences de l’exercice qui nous a été proposé de réaliser.


Certes, alors qu’il nous a été demandé de recueillir un fichier CSV passant par le langage PHP, nous avons fait le choix d’intégrer les technologies recherchées sur le marché afin de développer une analyse qui permette non seulement de fournir une réponse réelle au marché du travail, mais aussi de nous permettre de devenir force de proposition sur ce marché.


Le comparateur Google Trend et l’analyse des compétences recherchées sur les métiers du Big Data sont les moyens que nous avons utilisé en vue de connaître le besoin du marché du travail. De facto, nous avons commencé par suivre le résultat de cette brève étude pour faire notre choix ; nous avons également tenu à suivre l’exercice qui nous était demandé en sélectionnant NoSQL (couchdb) et PHP-Symfony car, ces technologies sont en parfaite adéquation avec le big data, mais surtout répondent favorablement aux conditions de l’exercice. 


Après de nombreuses tentatives, la mise en relation de la base de données couchdb et la version symfony 3.3 et 3.4 nous avons constaté l’absence de Bundle existant en vue de rendre effectif ce service. 


Par conséquent, pour rendre effective la mise en relation de notre base de données nous avons immigré vers un système de gestion de bases de données relationnelles à savoir MySQL.


## II. L'expression fonctionnelle du besoin :

Dans quel environnement notre objet fonctionnel est-il ? Quelle(s) fonction(s) doit-il réaliser ?

Notre besoin fonctionnel est de prédire les départs anticipés des salariés. Nous devons établir une corrélation entre la qualité de l’environnement de travail, la personnalité des salariés et le départ salarié.

Les technologies que nous utilisons sont le langage de programmation php, le framework symfony, la base de données mySQL. Par ailleurs nous avons également eu recourt à la librairie D3JS ainsi qu’au Machine Learning librairy for PHP.

### a) Définition fonctionnelle des technologies utilisées :
+ Nous avons obtenu un fichier CSV (comma-separated values) issue de l’administration de notre questionnaire sur Google Forms. Sa fonction est de stocker des données structurées. Ce fichier à servi pour l’importation des données au sein de MySQL.

+ Le SQL (Structured Query Language) est un langage informatique qui permet d'interagir avec des bases de données relationnelles. Grâce à ce dernier, la création de requête est possible et permet d’interagir avec les bases de données. En effet, MySQL est un SGBDR (à savoir un système de gestion de bases de données relationnelles). S’il nous a permis le stockage de nos données, il nous également permis d’avoir un espace de connexion, car nous l’avons employé comme le lieu au sein duquel a pu s’effectuer la mise en relation avec symfony. 

+ Symfony est un Framework PHP très populaire, français, et très utilisé dans le milieu des entreprises. Le 5 septembre 2017, Symfony passe la barre du milliard de téléchargement. Il permet aussi bien de réaliser des sites complexes que de proposer des services sur mesure, grâce au paramétrage des fonctionnalités de notre choix. Parmi les fonctions que propose Symfony, figure sa facilité d’intégration des modules de packages, la mise en relation des contrôleurs, les entités, les repository ainsi que les views. En sommes, cet outil présente une grande flexibilité d’utilisation.

+ PHP ou Hypertext Preprocessor, est un langage de programmation qui permet de faire du traitement automatisé de l’information sur un serveur Web et donc de faire des pages dynamiques.Il sert à traiter de l’information en interaction avec l’utilisateur pour générer du HTML sur un serveur Web (afficher). Du reste, PHP offre la possibilité de se connecter à une base de données, puis d’interagir avec cette dernière.

+ Librairie D3JS : il s’agit d’une bibliothèque graphique – JavaScript –. Elle permet l'affichage de données numériques sous une forme graphique mais également dynamique.

+ Twig est un moteur de templates pour le langage de programmation PHP, il est utilisé par défaut par le framework Symfony. L’intérêt de Twig au sein de notre étude consistait à pouvoir inscrire un langage de balisage html.

+ PHP-ai est une librairie open source en PHP, qui offre la possibilité d’utiliser plusieurs modèles d’algorithmes de Machine Learning.

+ Information facultative : Apache CouchDB est un système de gestion de base de données orienté documents, écrit en langage Erlang et distribué sous licence Apache.

### b) Définition des modèles algorithmiques employés.

+ La corrélation de Pearson :
le coefficient de corrélation mesure la relation linéaire ("proportionnalité") entre les valeurs de deux variables. Pour cela, elle suppose que les deux variables sont mesurées sur au moins une échelle d’intervalle.
Le coefficient de corrélation de Pearson se calcule comme suit: 
![image](/uploads/fd8b6201e199ad0df61dda0a5d1caf29/image.png)

+ Le KNN est une méthode d’apprentissage supervisé généralement utilisé pour estimer la sortie associée à une nouvelle entrée x, la méthode des k plus proches voisins consiste à prendre en compte (de façon identique) les k-échantillons d'apprentissage dont l’entrée est la plus proche de la nouvelle entrée x, selon une distance à définir.

+ Naive Bayes est un algorithme supervisé de Machine Learning, généralement utiliser pour une classification. Le model se base sur le théorème de Bayes. Ce dernier est un classique de la théorie des probabilités. Ce théorème est fondé sur les probabilités conditionnelles (voir ci-dessous). Il est possible de faire une classification avec un petit jeu de données. Le Naive Bayes est très rapide pour la classification, en effet les calculs de probabilités ne sont pas très coûteux.
![image](/uploads/5bbf059cb54fdd1bfe09063b7d2fed9b/image.png)

+ La machine à vecteur de support (SVM pour l’anglais support vector machine) est un outil d’apprentissage supervisé inventé par Vapnik en 1990. Le SVM a pour objectif de trouver la séparation entre deux classes d’objets avec l’idée que plus la séparation est large, plus la classification est robuste. L’algorithme sélectionne l’hyperplan qui sépare le jeu d’observations en deux classes distinctes de façon à maximiser la distance entre l’hyperplan et les observations les plus proches de l’échantillon d’apprentissage.

## III. Rétroplanning & Distribution des rôles

![image](/uploads/1bfdd809332a96a45bab8bb9985f735a/image.png)

## IV. Solution proposées et retour d’expérience
En effet, on commence ici à proposer des pistes de recherche pour la réalisation de chacune des difficultés rencontrées.	
### a) Difficulté dans la sélection du sujet : solution méthodologie

Devant la question qui nous a été posé : établir une corrélation de Pearson, et montrer l’évaluation d’une étude selon un ensemble de personne administrées, nous avons réfléchis au thème de notre sujet d’étude.

Un brainstorming nous a aidé à choisir le thème de notre étude, car nous avons arbitré entre une enquête sur l’utilisation des transports en commun – trajet domicile travail – en vue de prédire la prédisposition des usagers de parcours multimodaux à payer des services complémentaires (co-voiturage, taxi, vélib’, trottinette électrique). 

Toutefois, malgré la qualité et la nécessité d’un tel sujet, nous avons opté pour le traitement d’une problématique davantage ancré au cœur des problématiques entrepreneuriales. En l’occurrence, nous avons cherché à comprendre s’il existe d’éventuels facteurs responsables des départs anticipés des salariés à l’instar de facteurs exogènes tel que : la qualité de vie au travail ; et endogènes : tel que la personnalité des collaborateurs.

### b) Difficulté à mettre en relation Couchdb et Symfony : solution documentation.

Suite à la difficulté de mise en relation du système de gestion de base de données NoSQL (Not only SQL), qui d’ailleurs s’écarte du paradigme classique des bases de données relationnelles. Selon la version symfony 2, une documentation indique l’existence d’un Bundle permettant de rendre compatible couchdb (NoSQL) et Symfony 2.

La contrainte du temps seule, nous a empêché de réaliser cette nouvelle expérimentation afin de pouvoir intégrer couchdb au sein de notre projet. Nous nous sommes donc dirigés vers le SGBDR MySQL.

### c) Difficulté à maitriser l’usage Symfony : solution documentation & retour d’expériences des autres groupes.

La collaboration avec d’autres groupes et la recherche de tutoriels sur internet nous a permis de pallier les lacunes dans l’usage de cet outil.

## V. Résultat : ouverture

Nous avons plusieurs résultats :

Un environnement de travail de qualité offre des conditions d’épanouissement d’un salarié et détermine la pérennité du salarié en entreprise. Selon les résultats qu’attribuent les algorithmes du KNN, de Naive Bayes, ainsi que du SVM, il en ressort l’existence d’une forte corrélation entre qualité de l’environnement de travail et la durée d’un salarié au sein d’une entreprise. En effet, ce coefficient montre que plus la qualité de l’environnement de travail est bien évaluée par le salarié, plus le salarié évaluera sa durée en entreprise à la hausse. Inversement moins le salarié sera satisfait de son environnement de travail, moins la durée son contrat de travail durera au sein de son entreprise. 

Par conséquent, grâce à l’administration de notre questionnaire et à l’utilisation de nos algorithmes de prédiction, les entreprises seront en mesures d’optimiser la sélection à l'embauche. En effet, les ressources humaines seront en mesure de prononcer, à l’issue de la période d’essai du salarié, leur décision de garder ou non un salarié parmi leurs équipes, car à ce moment l’employé aura eu le temps d’évaluer la qualité de son environnement de travail. Nous recommandons donc, d’administrer le questionnaire qu’à l’issue de la période d’essai afin d’avoir la meilleure estimation possible, étant donné qu’elle est elle-même corrélée à une évaluation subjective. 


Notons de facto, que notre démarche de prédiction connaît une limite. Elle se base sur l’évaluation d’un ressenti, et ce sentiment est une donnée subjective. Elle est à pondérer à plusieurs moments, car l’état du salarié peut être variable durant une période donnée. Pour être efficace, il sera nécessaire de suivre cette procédure, qui n’en restera pas moins une estimation. 


En l’espèce, le meilleur axe d’amélioration de notre prédiction, selon notre préconisation est de proposer une évaluation des modèles que nous avons sélectionnés (KNN, Naive Bayes et SVM). Grâce à cela, il sera possible d’identifier et de justifier la pertinence d’un modèle avec une précision plus fine, et donc une prédiction plus proche de la réalité. 


En outre, l'amélioration de l'accompagnement des collaborateurs au sein de leur environnement de travail a grâce à notre modèle un champ d’application réel. En effet, après l’analyse des résultats de notre questionnaire, un responsable pourra identifier une tendance de ses employés, et définir une politique interne afin de corriger les éléments d’insatisfactions.

In fine, le coût que représente le départ d'un collaborateur sera effectivement réduit, un gain de temps, une économie financière et une optimisation de la coordination des équipes pourra être proposé. 

Notre test de personnalité comporte 9 profils différents. Nous avons inclus l’ensemble de ces profils à notre questionnaire. Aussi la corrélation de Pearson a utilisé l’ensemble de notre questionnaire. En sommes, nous n’avons pas pu approfondir notre enquête afin d’affirmer avec exactitude s’il existe une corrélation du profil de personnalité, liée à d’éventuels départs anticipés de salariés, mais nous avons réussi à produire un résultat de prédiction cohérent et vérifiable dans notre étude. Ceci montre qu’il existe une corrélation entre profil de personnalité et départ salariés. L’axe d’amélioration sera de définir les types de personnalités qui déterminent et impactent un départ salarié. 


### Corrélation de Pearson :
Dans la corrélation de Pearson, lorsque les résultats sont proches de 1, cela signifie que la prédiction donne à comprendre qu’il existe une corrélation parfaitement identique. Quand les résultats se rapprochent de la valeur 1, cela signifie que lorsque la valeur de l’un des deux augmente alors l’autre suit la même augmentation et inversement. Toutefois, lorsque les résultats sont proche de 0 voire égale à zéro cela indique qu’il n’y a pas de corrélation entre les deux valeurs. Enfin, lorsque les résultats sont proche de – 1 voire égale – 1, alors il s’agit de deux valeurs dont la corrélation est inverse. Autrement lorsqu’une des deux valeurs est décroissante, l’autre est croissante et réciproquement.

![image](/uploads/00758175c84943d1d7c2a34c4a77c14b/image.png)


https://gitlab.com/fhssirou/pred_emp

